require 'test_helper'

class AdminControllerTest < ActionController::TestCase
  test "should get general" do
    get :general
    assert_response :success
  end

  test "should get addEvento" do
    get :addEvento
    assert_response :success
  end

end
