require 'test_helper'

class FotolugarsControllerTest < ActionController::TestCase
  setup do
    @fotolugar = fotolugars(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fotolugars)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fotolugar" do
    assert_difference('Fotolugar.count') do
      post :create, fotolugar: { descricao: @fotolugar.descricao, foto: @fotolugar.foto }
    end

    assert_redirected_to fotolugar_path(assigns(:fotolugar))
  end

  test "should show fotolugar" do
    get :show, id: @fotolugar
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fotolugar
    assert_response :success
  end

  test "should update fotolugar" do
    put :update, id: @fotolugar, fotolugar: { descricao: @fotolugar.descricao, foto: @fotolugar.foto }
    assert_redirected_to fotolugar_path(assigns(:fotolugar))
  end

  test "should destroy fotolugar" do
    assert_difference('Fotolugar.count', -1) do
      delete :destroy, id: @fotolugar
    end

    assert_redirected_to fotolugars_path
  end
end
