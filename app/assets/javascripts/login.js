$(document).ready(function(){
  $('#login-trigger').click(function(){
    $(this).next('#login-content').slideToggle();
    $(this).toggleClass('active');          
    
    if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
    else $(this).find('span').html('&#x25BC;')
  })
});

$(document).ready(function(){
 setInterval(function() {
  $("input[type=text]").each(function() {
   var element = $(this);
   if (element.val() !== "") {
     $(this).css({
       boxShadow: 'inset 8px 0px 0  #E58621',
       paddingLeft: '12px'})
   }
   var element = $(this);
   if (element.val() == "") {
     $(this).css('border-left', '1px solid #E58621')
   }
 });  
}, 200);
}); 

$(document).ready(function(){
 setInterval(function() {
  $("input[type=password]").each(function() {
   var element = $(this);
   if (element.val() !== "") {
     $(this).css({
       boxShadow: 'inset 8px 0px 0  #E58621',
       paddingLeft: '12px'})
   }
   var element = $(this);
   if (element.val() == "") {
     $(this).css('border-left', '1px solid #E58621')
   }
 });  
}, 200);
}); 

function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('password_r');
    var pass2 = document.getElementById('password_r1');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    var btn = document.getElementById('registrationdone');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(pass1.value == pass2.value){
      btn.disabled = false;
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
      }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        
        btn.disabled = true;
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
      }
    }  

    function checkPass2()
    {
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('edit2');
    var pass2 = document.getElementById('edit2_c');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    var btn = document.getElementById('buttons2');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(pass1.value == pass2.value){
      btn.disabled = false;
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
      }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        
        btn.disabled = true;
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
      }
    }  

