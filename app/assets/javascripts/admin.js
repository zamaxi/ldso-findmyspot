jQuery(document).ready(function(){
	jQuery('#datetimepicker').datetimepicker();
	$("#submit_photo").attr("disabled", true);

	$('#imagelink').attr("disabled", true);
});

$(function() {
	$("input:file").change(function (){
		if($("#fotosector_sector_id").val() != 0){
			$("#submit_photo").attr("disabled", false);
		}
	});
});

jQuery(function() {
	var states;
	states = $("#_recinto_id").html();
	$("#venue_select").change(function() {
		var country, options;
		country = $("#venue_select :selected").text();
		options = $(states).filter('optgroup[label="' + country + '"]').html();
		if (options) {
			$("#_recinto_id").html(options);
		}
		else
		{
			$("#_recinto_id").empty();
		}
	});
	var states2;
	states2 = $("#_sector_id").html();
	$("#_recinto_id").change(function() {
		var country, options;
		country = $("#_recinto_id :selected").text();
		
		options = $(states2).filter('optgroup[label="' + country + '"]').html();
		if (options) {
			$("#_sector_id").html(options);
		}
		else
		{
			$("#_sector_id").empty();
		}
	});
	$("#venue_select").change(function() {
		var country, options;
		country = $("#_recinto_id :selected").text();
		options = $(states2).filter('optgroup[label="' + country + '"]').html();
		if (options) {
			$("#_sector_id").html(options);
		}
		else
		{
			$("#_sector_id").empty();

		}
	});
});

$(document).on('change','#venue_select',function(){
	var value;
	value =  $("select#venue_select option:selected").val();

	if (value == 1){
		$("#setores").show("slow");
		$("#fotos").show("slow");
		$('#teste').show("slow");

	} else if (value == 2){
		$("#setores").show("slow");
		$("#fotos").show("slow");
		$('#teste').show("slow");
	}
	else if (value == 3){
		$("#setores").show("slow");
		$("#fotos").show("slow");
		$('#teste').show("slow");
	}
	else{
		$("#setores").slideUp();
		$("#fotos").slideUp();
		$('#teste').slideUp();;
	}
	
});


$(document).on('change','#_recinto_id',function(){
	if( $('#_sector_id').has('option').length > 0 ) {
		$("#fotos").show("slow");
	}
	else{
		$("#fotos").slideUp();
	}

});

$(document).on('change','#_sector_id',function(){
	var a = $("#_sector_id :selected").text();
	var b = $("#_sector_id :selected").val();
	var c = $("#_recinto_id :selected").val();
	$('#teste').attr( 'src' , '../assets/'+a);
	$('#teste').attr( 'width' , '105%');
	$('#imagelink').attr( 'href' , '/fotosectors/'+b);
	$('#fotosector_sector_id').val(c);

	if($("#fotosector_sector_id").val() != 0){
		if($("#file").val() != ''){
			$("#submit_photo").attr("disabled", false);
		}	
	}
	else{
		$("#submit_photo").attr("disabled", true);
	}
});

$(document).on('change','#_recinto_id',function(){
	var a = $("#_sector_id :selected").text();
	var b = $("#_sector_id :selected").val();
	var c = $("#_recinto_id :selected").val();
	$('#teste').attr( 'src' , '../assets/'+a);
	$('#teste').attr( 'width' , '105%');
	$('#fotosector_sector_id').val(c);	

	if($("#fotosector_sector_id").val() != 0){
		if($("#file").val() != ''){
			$("#submit_photo").attr("disabled", false);
		}	
	}
	else{
		$("#submit_photo").attr("disabled", true);
	}
});

$(document).on('change','#venue_select',function(){
	var a = $("#_sector_id :selected").text();
	var b = $("#_sector_id :selected").val();
	var c = $("#_recinto_id :selected").val();
	$('#teste').attr( 'src' , '../assets/'+a);
	$('#teste').attr( 'width' , '105%');
	$('#fotosector_sector_id').val(c);

	if($("#fotosector_sector_id").val() != 0){
		if($("#file").val() != ''){
			$("#submit_photo").attr("disabled", false);
		}	
	}
	else{
		$("#submit_photo").attr("disabled", true);
	}
});