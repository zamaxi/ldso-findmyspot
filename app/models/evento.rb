class Evento < ActiveRecord::Base
  attr_accessible :date, :description, :title, :promotor, :url, :recinto_id
  belongs_to :recinto
end
