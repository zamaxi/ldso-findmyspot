class Sector < ActiveRecord::Base
  attr_accessible :descritivo, :distancia_bar, :distancia_wc, :recinto_id
  belongs_to :recinto
  has_many :fotosector
  has_many :comentarios
  has_many :fotolugars
end
