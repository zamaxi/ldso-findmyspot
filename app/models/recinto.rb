class Recinto < ActiveRecord::Base
  attr_accessible :capacidade, :morada, :nome, :notas, :telefone
  has_many :eventos
  has_many :sectors
end
