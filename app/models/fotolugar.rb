class Fotolugar < ActiveRecord::Base
  attr_accessible :descricao, :foto, :sector_id, :user_ids
  belongs_to :user
  belongs_to :sector
end
