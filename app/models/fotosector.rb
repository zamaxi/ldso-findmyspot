class Fotosector < ActiveRecord::Base
  attr_accessible :foto, :sector_id, :nmr_checkins, :nmr_ratings, :rating
  belongs_to :sector
  has_many :comentario
  has_many :upvotes
  has_and_belongs_to_many :users
end
