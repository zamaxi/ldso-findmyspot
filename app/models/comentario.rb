class Comentario < ActiveRecord::Base
  attr_accessible :corpo, :user_id, :sector_id, :user_id, :fotosector_id, :fotopath
  belongs_to :user
  belongs_to :fotosector
  belongs_to :sector
end
