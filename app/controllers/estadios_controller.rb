class EstadiosController < ApplicationController
  before_filter :set_estadio, only: [:show, :edit, :update, :destroy]

  def index
    @estadios = Estadio.all
    respond_with(@estadios)
  end

  def show
    respond_with(@estadio)
  end

  def new
    @estadio = Estadio.new
    respond_with(@estadio)
  end

  def edit
  end

  def create
    @estadio = Estadio.new(params[:estadio])
    @estadio.save
    respond_with(@estadio)
  end

  def update
    @estadio.update_attributes(params[:estadio])
    respond_with(@estadio)
  end

  def destroy
    @estadio.destroy
    respond_with(@estadio)
  end

  private
    def set_estadio
      @estadio = Estadio.find(params[:id])
    end
end
