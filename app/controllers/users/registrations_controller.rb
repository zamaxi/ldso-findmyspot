class Users::RegistrationsController < Devise::RegistrationsController
    def update
    # Search user by id
    @user = User.find(current_user.id)
    @lol = "aaaaa";
    # Verify if we need to request current password from user
    successfully_updated = if needs_password?(@user, params)
                             @user.update_with_password(params[:email])
                           else
                             # Remove the virtual current_password attribute
                             # update_without_password
                             params[:email].delete(:current_password)
                             @user.update_without_password(params[:email])
                           end

    if successfully_updated
      set_flash_message :notice, :updated
      # Sign in the user bypassing validation in case his password changed
      sign_in @user, :bypass => true
      redirect_to after_update_path_for(@user)
    else
      render "edit"
    end
  end

    private

  def needs_password?(user, params)
    # Verify if email changed
    user.email != params[:email] ||
        # Verify if the password has been informed
        params[:email][:password].present? ||
        # Verify if username changed
        user.username != params[:email]
  end
end