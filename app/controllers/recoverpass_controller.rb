class RecoverpassController < ApplicationController
	def recover
	end

	def change
		pass = params[:p]
   		mail = params[:m]

   		user = User.where(email: mail).first
		user.password = pass
  		user.save

  		redirect_to root_path, notice: "Your password has been changed!"
	end
end
