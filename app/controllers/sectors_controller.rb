class SectorsController < ApplicationController
  before_filter :set_sector, only: [:show, :edit, :update, :destroy]
  def index
    @sectors = Sector.all
    respond_with(@sectors)
  end

  def show
    respond_with(@sector)
  end

  def new
    @sector = Sector.new
    respond_with(@sector)
  end

  def edit
  end

  def create
    @sector = Sector.new(params[:sector])
    @sector.save
    respond_with(@sector)
  end

  def update
    @sector.update_attributes(params[:sector])
    respond_with(@sector)
  end

  def destroy
    @sector.destroy
    respond_with(@sector)
  end

  private
    def set_sector
      @sector = Sector.find(params[:id])
    end
end
