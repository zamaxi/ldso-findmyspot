class EventosController < ApplicationController
  before_filter :set_evento, only: [:show, :edit, :update, :destroy]

  def index
    redirect_to('/admin/destroy')
    @eventos = Evento.all
  end

  def show
    redirect_to('/admin/general')
  end

  def new
    @evento = Evento.new
    respond_with(@evento)
  end

  def edit
  end

  def create
    @evento = Evento.new(params[:evento])
    @evento.save
    respond_with(@evento)
  end

  def update
    @evento.update_attributes(params[:evento])
    respond_with(@evento)
  end

  def destroy
    @evento.destroy
    respond_with(@evento)
  end

  private
  def set_evento
    @evento = Evento.find(params[:id])
  end
end
