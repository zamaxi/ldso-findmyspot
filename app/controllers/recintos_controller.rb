class RecintosController < ApplicationController
  before_filter :set_recinto, only: [:show, :edit, :update, :destroy]

  def index
    @recintos = Recinto.all
    respond_with(@recintos)
  end

  def show
    respond_with(@recinto)
  end

  def new
    @recinto = Recinto.new
    respond_with(@recinto)
  end

  def edit
  end

  def create
    @recinto = Recinto.new(params[:recinto])
    @recinto.save
    respond_with(@recinto)
  end

  def update
    @recinto.update_attributes(params[:recinto])
    respond_with(@recinto)
  end

  def destroy
    @recinto.destroy
    respond_with(@recinto)
  end

  private
    def set_recinto
      @recinto = Recinto.find(params[:id])
    end
end
