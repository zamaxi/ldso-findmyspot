class FotosectorsController < ApplicationController
  before_filter :set_fotosector, only: [:show, :edit, :update, :destroy]

  def index
   @fotosectors = Fotosector.all
   redirect_to('/admin/general')
 end

 def show
  respond_with(@fotosector)
end

def new
  @fotosector = Fotosector.new
  respond_with(@fotosector)
end

def edit
end

def create

  file = params[:file]
  @fotosector = Fotosector.new(params[:fotosector])
  @fotosector.foto = file.original_filename
  @fotosector.save
  #respond_with(@fotosector)

  path = "app/assets/images/"+ @fotosector.foto

  File.open(path, "wb") do |f|
    f.write(file.read)
  end

  respond_to do |format|
    format.js
    format.html
  end
end

def update
  @fotosector.update_attributes(params[:fotosector])
  respond_with(@fotosector)
end

def destroy
  @fotosector.destroy
  respond_with(@fotosector)
end

private
def set_fotosector
  @fotosector = Fotosector.find(params[:id])
end
end
