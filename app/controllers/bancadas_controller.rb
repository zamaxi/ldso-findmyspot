class BancadasController < ApplicationController
  before_filter :set_bancada, only: [:show, :edit, :update, :destroy]

  def index
    @bancadas = Bancada.all
    respond_with(@bancadas)
  end

  def show
    respond_with(@bancada)
  end

  def new
    @bancada = Bancada.new
    respond_with(@bancada)
  end

  def edit
  end

  def create
    @bancada = Bancada.new(params[:bancada])
    @bancada.save
    respond_with(@bancada)
  end

  def update
    @bancada.update_attributes(params[:bancada])
    respond_with(@bancada)
  end

  def destroy
    @bancada.destroy
    respond_with(@bancada)
  end

  private
    def set_bancada
      @bancada = Bancada.find(params[:id])
    end
end
