class WidgetsController < ApplicationController
  before_filter :set_widget, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @widgets = Widget.all
    respond_with(@widgets)
  end

  def show
    respond_with(@widget)
  end

  def new
    @widget = Widget.new
    respond_with(@widget)
  end

  def edit
  end

  def create
    @widget = Widget.new(params[:widget])
    @widget.save
    respond_with(@widget)
  end

  def update
    @widget.update_attributes(params[:widget])
    respond_with(@widget)
  end

  def destroy
    @widget.destroy
    respond_with(@widget)
  end

  private
    def set_widget
      @widget = Widget.find(params[:id])
    end
end
