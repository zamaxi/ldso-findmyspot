class ApplicationController < ActionController::Base
  protect_from_forgery
  respond_to :html, :json
  responders :flash
  include SessionsHelper 
  
  helper_method :current_user

  def create
    UserNotifier.send_signup_email(User.find.last).deliver
    user = User.from_omniauth(env["omniauth.auth"])
    session[:user_id] = user.id
    redirect_to root_path
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_path
  end

  def admin?
    current_user.email == "admin@admin.com"
  end

  def banned?
    current_user.banned == true
  end

  def require_admin
    unless current_user && admin?
      redirect_to root_path
    end        
  end

  def page_not_found
    respond_to do |format|
      format.html { render template: 'errors/not_found_error', layout: 'layouts/application', status: 404 }
      format.all  { render nothing: true, status: 404 }
    end
  end

end