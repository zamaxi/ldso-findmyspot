class AdminController < ApplicationController
	before_filter :authenticate_user!
	before_filter :require_admin
	def general
		@fotosector = Fotosector.new
		
		@recintos = Recinto.all
		@sectores = Sector.all
		respond_with(@fotosector)
	end

	def addEvento
		@to_destroy = 3
		cookies[:name]=params[:selected_value]
		@evento = Evento.new
		respond_with('create')
	end

	def destroy
		@datetoday = DateTime.now
		@eventos = Evento.all
		respond_with(@evento)
	end

	def create
		redirect_to('/404')
	end

	def user
		word = params[:n]
		ban_id = params[:b]
		@word = params[:n]

		if word != nil
			@usersResult = User.where("name LIKE '%"+word+"%'")
		end	
		if ban_id != nil
			user = User.find(ban_id.to_f)
			if user.banned == true
				user.banned = false
				user.save
				flash[:notice] = "User successfully disbanned"
			else 
				user.banned = true
				user.save
				flash[:notice] = "User successfully banned"
			end 
		end	
	end

	def userSearch
	end

	def konami
	end
end