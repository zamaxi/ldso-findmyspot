class LugarsController < ApplicationController
  before_filter :set_lugar, only: [:show, :edit, :update, :destroy]

  def index
    @lugars = Lugar.all
    respond_with(@lugars)
  end

  def show
    respond_with(@lugar)
  end

  def new
    @lugar = Lugar.new
    respond_with(@lugar)
  end

  def edit
  end

  def create
    @lugar = Lugar.new(params[:lugar])
    @lugar.save
    respond_with(@lugar)
  end

  def update
    @lugar.update_attributes(params[:lugar])
    respond_with(@lugar)
  end

  def destroy
    @lugar.destroy
    respond_with(@lugar)
  end

  private
    def set_lugar
      @lugar = Lugar.find(params[:id])
    end
end
