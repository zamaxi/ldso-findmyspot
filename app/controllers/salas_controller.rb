class SalasController < ApplicationController
  before_filter :set_sala, only: [:show, :edit, :update, :destroy]
  def index
    @salas = Sala.all
    respond_with(@salas)
  end

  def show
    respond_with(@sala)
  end

  def new
    @sala = Sala.new
    respond_with(@sala)
  end

  def edit
  end

  def create
    @sala = Sala.new(params[:sala])
    @sala.save
    respond_with(@sala)
  end

  def update
    @sala.update_attributes(params[:sala])
    respond_with(@sala)
  end

  def destroy
    @sala.destroy
    respond_with(@sala)
  end

  private
    def set_sala
      @sala = Sala.find(params[:id])
    end
end
