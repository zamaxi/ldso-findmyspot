class FotolugarsController < ApplicationController
  before_filter :set_fotolugar, only: [:show, :edit, :update, :destroy]

  def index
    @fotolugars = Fotolugar.all
    respond_with(@fotolugars)
  end

  def show
    respond_with(@fotolugar)
  end

  def new
    @fotolugar = Fotolugar.new
    respond_with(@fotolugar)
  end

  def edit
  end

  def create
    @fotolugar = Fotolugar.new(params[:fotolugar])
    @fotolugar.save
    respond_with(@fotolugar)
  end

  def update
    @fotolugar.update_attributes(params[:fotolugar])
    respond_with(@fotolugar)
  end

  def destroy
    @fotolugar.destroy
    respond_with(@fotolugar)
  end

  private
    def set_fotolugar
      @fotolugar = Fotolugar.find(params[:id])
    end
end
