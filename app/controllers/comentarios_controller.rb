class ComentariosController < ApplicationController
  before_filter :set_comentario, only: [:show, :edit, :update, :destroy]

  def index
    @comentarios = Comentario.all
	@fotosectors = Fotosector.all
    @comentario = Comentario.new
    @sector_id = params[:sector]
	@upvotes = Upvote.all
    @fotosector_id = params[:fotosector]
    respond_with(@comentarios)
	@commented = 0
	
  end

  def show
    respond_with(@comentario)
  end

  def new
    @comentario = Comentario.new
    respond_with(@comentario)
  end

  def edit
  end

  def create
    if params[:file] != nil
      file = params[:file]

      @comentario = Comentario.create(params[:comentario])
      @comentario.fotopath = @comentario.id.to_s + '.jpg'

      path = "app/assets/images/"+ @comentario.id.to_s  + '.jpg'
      File.open(path, "wb") do |f|
        f.write(file.read)
      end
      
      respond_to do |format|
        if @comentario.save
          format.js
        end
          format.html
      end

    else 
      @comentario = Comentario.new(params[:comentario])
      @comentario.save
      respond_to do |format|
        format.js
        format.html
      end
    end

  end

  def update
    @comentario.update_attributes(params[:comentario])
    respond_with(@comentario)
  end

  def destroy
    @comentario.destroy
    respond_with(@comentario)
  end
  

  def del_comentario
  id = params[:id_c]
  
	@comentario = Comentario.find(id.to_f)
	
	@comentario.destroy
		
	
	redirect_to :back
	
  end
  
  def rating_display
  id_s = params[:id_sec]
  
  @fotosects = Fotosector.find(id_s)
  
  @fotosects.rating =  @fotosects.rating + 1
  
  @fotosects.save

  
  
  upvote = Upvote.create
  upvote.user_id = current_user.id
  upvote.fotosector_id = id_s
  
  upvote.save
	
  
  
  redirect_to :back
  
  
  end
  
  
  def downvote
  id_s = params[:id_sec]
  id_us = params[:id_user]
  
  @fotosects = Fotosector.find(id_s)
  
  @fotosects.rating =  @fotosects.rating - 1
  
  @fotosects.save

 @downvote = Upvote.where(:user_id=>params[:id_user]).where(:fotosector_id=>params[:id_sec]).first
  
  @downvote.destroy
	
  
  
  redirect_to :back
  
  
  end
  
  
  
  
  private
  def set_comentario
    @comentario = Comentario.find(params[:id])
  end
  

end
