class UpvotesController < ApplicationController
  before_filter :set_upvote, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @upvotes = Upvote.all
    respond_with(@upvotes)
  end

  def show
    respond_with(@upvote)
  end

  def new
    @upvote = Upvote.new
    respond_with(@upvote)
  end

  def edit
  end

  def create
    @upvote = Upvote.new(params[:upvote])
    @upvote.save
    respond_with(@upvote)
  end

  def update
    @upvote.update_attributes(params[:upvote])
    respond_with(@upvote)
  end

  def destroy
    @upvote.destroy
    respond_with(@upvote)
  end

  private
    def set_upvote
      @upvote = Upvote.find(params[:id])
    end
end
