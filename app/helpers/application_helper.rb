module ApplicationHelper
	def admin?
		current_user.email == "admin@admin.com"
	end

	def banned?
		current_user.banned == true
	end
end
