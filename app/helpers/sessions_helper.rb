module SessionsHelper
 def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
  
  def deny_access
    redirect_to "/404"
  end

end
