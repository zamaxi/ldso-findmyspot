class CreateSectors < ActiveRecord::Migration
  def change
    create_table :sectors do |t|
      t.text :descritivo
      t.integer :distancia_bar
      t.integer :distancia_wc

      t.timestamps
    end
  end
end
