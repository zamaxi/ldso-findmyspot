class ChangeEventsAts < ActiveRecord::Migration
  def change
    rename_column :eventos, :nome, :title
	rename_column :eventos, :info, :description
	add_column :eventos, :url, :string
  end
end
