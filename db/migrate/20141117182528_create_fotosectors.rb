class CreateFotosectors < ActiveRecord::Migration
  def change
    create_table :fotosectors do |t|
      t.text :foto

      t.timestamps
    end
  end
end
