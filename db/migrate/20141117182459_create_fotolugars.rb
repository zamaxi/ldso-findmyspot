class CreateFotolugars < ActiveRecord::Migration
  def change
    create_table :fotolugars do |t|
      t.text :foto
      t.text :descricao

      t.timestamps
    end
  end
end
