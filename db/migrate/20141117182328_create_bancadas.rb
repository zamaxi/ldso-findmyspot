class CreateBancadas < ActiveRecord::Migration
  def change
    create_table :bancadas do |t|
      t.text :nome

      t.timestamps
    end
  end
end
