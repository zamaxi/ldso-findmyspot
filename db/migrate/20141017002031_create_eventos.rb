class CreateEventos < ActiveRecord::Migration
  def change
    create_table :eventos do |t|
      t.string :nome
      t.text :info
      t.datetime :data
      t.string :promotor

      t.timestamps
    end
  end
end
