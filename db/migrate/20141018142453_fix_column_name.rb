class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :eventos, :recindo_id, :recinto_id
  end
end