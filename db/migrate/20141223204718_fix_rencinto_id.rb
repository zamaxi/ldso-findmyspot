class FixRencintoId < ActiveRecord::Migration
   def self.up
    rename_column :sectors, :recindo_id, :recinto_id
  end

  def self.down
    # rename back if you need or do something else or do nothing
  end
end