class CreateRecintos < ActiveRecord::Migration
  def change
    create_table :recintos do |t|
      t.string :nome
      t.integer :capacidade
      t.string :morada
      t.text :notas
      t.string :telefone

      t.timestamps
    end
  end
end
