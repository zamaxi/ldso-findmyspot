class AddCheckRatFs < ActiveRecord::Migration
  def change
  	add_column :fotosectors, :nmr_checkins, :integer
  	add_column :fotosectors, :nmr_ratings, :integer
  	add_column :fotosectors, :rating, :integer
  end
end
