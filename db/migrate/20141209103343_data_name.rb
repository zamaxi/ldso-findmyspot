class DataName < ActiveRecord::Migration
  def change
    rename_column :eventos, :data, :date
  end
end
