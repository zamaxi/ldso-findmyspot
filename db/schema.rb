# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150106042236) do

  create_table "bancadas", :force => true do |t|
    t.text     "nome"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "comentarios", :force => true do |t|
    t.text     "corpo"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "sector_id"
    t.integer  "user_id"
    t.integer  "fotosector_id"
    t.string   "fotopath"
  end

  create_table "estadios", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "eventos", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "date"
    t.string   "promotor"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "recinto_id"
    t.integer  "recindo_id"
    t.string   "url"
  end

  create_table "fotolugars", :force => true do |t|
    t.text     "foto"
    t.text     "descricao"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "sector_id"
    t.integer  "user_id"
  end

  create_table "fotosectors", :force => true do |t|
    t.text     "foto"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "sector_id"
    t.integer  "nmr_checkins"
    t.integer  "nmr_ratings"
    t.integer  "rating"
  end

  create_table "lugars", :force => true do |t|
    t.text     "descritivo"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "recintos", :force => true do |t|
    t.string   "nome"
    t.integer  "capacidade"
    t.string   "morada"
    t.text     "notas"
    t.string   "telefone"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "salas", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "sectors", :force => true do |t|
    t.text     "descritivo"
    t.integer  "distancia_bar"
    t.integer  "distancia_wc"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "recinto_id"
  end

  create_table "upvotes", :force => true do |t|
    t.integer  "user_id"
    t.integer  "fotosector_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0,     :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "provider"
    t.string   "uid"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.string   "name"
    t.string   "photo"
    t.string   "password"
    t.string   "password_confirmation"
    t.boolean  "banned",                 :default => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "widgets", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
