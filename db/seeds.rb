#Setores
Sector.destroy_all
ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'sectors'")
Sector.create(descritivo: "S01", recinto_id:1)
Sector.create(descritivo: "S02", recinto_id:1)
Sector.create(descritivo: "S03", recinto_id:1)
Sector.create(descritivo: "S04", recinto_id:1)
Sector.create(descritivo: "S05", recinto_id:1)
Sector.create(descritivo: "S06", recinto_id:1)
Sector.create(descritivo: "S07", recinto_id:1)
Sector.create(descritivo: "S08", recinto_id:1)
Sector.create(descritivo: "S09", recinto_id:1)
Sector.create(descritivo: "S10", recinto_id:1)
Sector.create(descritivo: "S11", recinto_id:1)
Sector.create(descritivo: "S12", recinto_id:1)
Sector.create(descritivo: "S13", recinto_id:1)
Sector.create(descritivo: "S14", recinto_id:1)
Sector.create(descritivo: "S15", recinto_id:1)
Sector.create(descritivo: "S16", recinto_id:1)
Sector.create(descritivo: "S17", recinto_id:1)
Sector.create(descritivo: "S18", recinto_id:1)
Sector.create(descritivo: "S19", recinto_id:1)
Sector.create(descritivo: "S20", recinto_id:1)
Sector.create(descritivo: "S21", recinto_id:1)
Sector.create(descritivo: "S22", recinto_id:1)
Sector.create(descritivo: "S23", recinto_id:1)
Sector.create(descritivo: "S24", recinto_id:1)
Sector.create(descritivo: "S25", recinto_id:1)
Sector.create(descritivo: "S26", recinto_id:1)
Sector.create(descritivo: "S27", recinto_id:1)
Sector.create(descritivo: "S28", recinto_id:1)
Sector.create(descritivo: "S29", recinto_id:1)
Sector.create(descritivo: "S30", recinto_id:1)
Sector.create(descritivo: "S31", recinto_id:1)
Sector.create(descritivo: "S32", recinto_id:1)
Sector.create(descritivo: "S33", recinto_id:1)
Sector.create(descritivo: "S34", recinto_id:1)
Sector.create(descritivo: "S35", recinto_id:1)
Sector.create(descritivo: "S36", recinto_id:1)
Sector.create(descritivo: "S38", recinto_id:1)
Sector.create(descritivo: "S39", recinto_id:1)
Sector.create(descritivo: "S40", recinto_id:1)
Sector.create(descritivo: "S41", recinto_id:1)
Sector.create(descritivo: "S42", recinto_id:1)
Sector.create(descritivo: "S43", recinto_id:1)
Sector.create(descritivo: "S44", recinto_id:1)
Sector.create(descritivo: "S45", recinto_id:1)
Sector.create(descritivo: "S46", recinto_id:1)
Sector.create(descritivo: "S47", recinto_id:1)
Sector.create(descritivo: "S48", recinto_id:1)
Sector.create(descritivo: "S49", recinto_id:1)
Sector.create(descritivo: "S50", recinto_id:1)

#Sectores Auditorio FEUP
Sector.create(descritivo: "A01", recinto_id:2)
Sector.create(descritivo: "A02", recinto_id:2)
Sector.create(descritivo: "B01", recinto_id:2)
Sector.create(descritivo: "B02", recinto_id:2)
Sector.create(descritivo: "C01", recinto_id:2)
Sector.create(descritivo: "C02", recinto_id:2)
#Fotosectors
Fotosector.destroy_all
ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'fotosectors'")

#bancada SB
Fotosector.create(foto:"s06b.jpg", sector_id:6)
Fotosector.create(foto:"s06m.jpg", sector_id:6)
Fotosector.create(foto:"s06c.jpg", sector_id:6)

Fotosector.create(foto:"s07b.jpg", sector_id:7)
Fotosector.create(foto:"s07m.jpg", sector_id:7)
Fotosector.create(foto:"s07c.jpg", sector_id:7)

Fotosector.create(foto:"s08b.jpg", sector_id:8)
Fotosector.create(foto:"s08m.jpg", sector_id:8)
Fotosector.create(foto:"s08c.jpg", sector_id:8)

Fotosector.create(foto:"s09b.jpg", sector_id:9)
Fotosector.create(foto:"s09m.jpg", sector_id:9)
Fotosector.create(foto:"s09c.jpg", sector_id:9)

Fotosector.create(foto:"s10b.jpg", sector_id:10)
Fotosector.create(foto:"s10m.jpg", sector_id:10)
Fotosector.create(foto:"s10c.jpg", sector_id:10)

Fotosector.create(foto:"s11b.jpg", sector_id:11)
Fotosector.create(foto:"s11m.jpg", sector_id:11)
Fotosector.create(foto:"s11c.jpg", sector_id:11)

Fotosector.create(foto:"s12b.jpg", sector_id:12)
Fotosector.create(foto:"s12m.jpg", sector_id:12)
Fotosector.create(foto:"s12c.jpg", sector_id:12)

#Bancada MEO
Fotosector.create(foto:"s01b.jpg", sector_id:1)
Fotosector.create(foto:"s01m.jpg", sector_id:1)
Fotosector.create(foto:"s01c.jpg", sector_id:1)

Fotosector.create(foto:"s02b.jpg", sector_id:2)
Fotosector.create(foto:"s02m.jpg", sector_id:2)
Fotosector.create(foto:"s02c.jpg", sector_id:2)

Fotosector.create(foto:"s03b.jpg", sector_id:3)
Fotosector.create(foto:"s03m.jpg", sector_id:3)
Fotosector.create(foto:"s03c.jpg", sector_id:3)

Fotosector.create(foto:"s04b.jpg", sector_id:4)
Fotosector.create(foto:"s04m.jpg", sector_id:4)
Fotosector.create(foto:"s04c.jpg", sector_id:4)

Fotosector.create(foto:"s05b.jpg", sector_id:5)
Fotosector.create(foto:"s05m.jpg", sector_id:5)
Fotosector.create(foto:"s05c.jpg", sector_id:5)

Fotosector.create(foto:"s29b.jpg", sector_id:29)
Fotosector.create(foto:"s29m.jpg", sector_id:29)
Fotosector.create(foto:"s29c.jpg", sector_id:29)

Fotosector.create(foto:"s30b.jpg", sector_id:30)
Fotosector.create(foto:"s30m.jpg", sector_id:30)
Fotosector.create(foto:"s30c.jpg", sector_id:30)

Fotosector.create(foto:"s31b.jpg", sector_id:31)
Fotosector.create(foto:"s31m.jpg", sector_id:31)
Fotosector.create(foto:"s31c.jpg", sector_id:31)

Fotosector.create(foto:"s32b.jpg", sector_id:32)
Fotosector.create(foto:"s32m.jpg", sector_id:32)
Fotosector.create(foto:"s32c.jpg", sector_id:32)

Fotosector.create(foto:"s33b.jpg", sector_id:33)
Fotosector.create(foto:"s33c.jpg", sector_id:33)

Fotosector.create(foto:"s34b.jpg", sector_id:34)
Fotosector.create(foto:"s34c.jpg", sector_id:34)

Fotosector.create(foto:"s35b.jpg", sector_id:35)
Fotosector.create(foto:"s35c.jpg", sector_id:35)

Fotosector.create(foto:"s36b.jpg", sector_id:36)
Fotosector.create(foto:"s36c.jpg", sector_id:36)

Fotosector.create(foto:"s38b.jpg", sector_id:37)
Fotosector.create(foto:"s38c.jpg", sector_id:37)

Fotosector.create(foto:"s39b.jpg", sector_id:38)
Fotosector.create(foto:"s39c.jpg", sector_id:38)

Fotosector.create(foto:"s40b.jpg", sector_id:39)
Fotosector.create(foto:"s40c.jpg", sector_id:39)

Fotosector.create(foto:"s41b.jpg", sector_id:40)
Fotosector.create(foto:"s41c.jpg", sector_id:40)

#Bancada CC
Fotosector.create(foto:"s28b.jpg", sector_id:28)
Fotosector.create(foto:"s28m.jpg", sector_id:28)
Fotosector.create(foto:"s28c.jpg", sector_id:28)

Fotosector.create(foto:"s27b.jpg", sector_id:27)
Fotosector.create(foto:"s27m.jpg", sector_id:27)
Fotosector.create(foto:"s27c.jpg", sector_id:27)

Fotosector.create(foto:"s26b.jpg", sector_id:26)
Fotosector.create(foto:"s26m.jpg", sector_id:26)
Fotosector.create(foto:"s26c.jpg", sector_id:26)

Fotosector.create(foto:"s25b.jpg", sector_id:25)
Fotosector.create(foto:"s25m.jpg", sector_id:25)
Fotosector.create(foto:"s25c.jpg", sector_id:25)

Fotosector.create(foto:"s24b.jpg", sector_id:24)
Fotosector.create(foto:"s24m.jpg", sector_id:24)
Fotosector.create(foto:"s24c.jpg", sector_id:24)

Fotosector.create(foto:"s23b.jpg", sector_id:23)
Fotosector.create(foto:"s23m.jpg", sector_id:23)
Fotosector.create(foto:"s23c.jpg", sector_id:23)

Fotosector.create(foto:"s22b.jpg", sector_id:22)
Fotosector.create(foto:"s22m.jpg", sector_id:22)
Fotosector.create(foto:"s22c.jpg", sector_id:22)

#Bancada MOCHE
Fotosector.create(foto:"s21b.jpg", sector_id:21)
Fotosector.create(foto:"s21m.jpg", sector_id:21)
Fotosector.create(foto:"s21c.jpg", sector_id:21)

Fotosector.create(foto:"s20b.jpg", sector_id:20)
Fotosector.create(foto:"s20m.jpg", sector_id:20)
Fotosector.create(foto:"s20c.jpg", sector_id:20)

Fotosector.create(foto:"s19b.jpg", sector_id:19)
Fotosector.create(foto:"s19m.jpg", sector_id:19)
Fotosector.create(foto:"s19c.jpg", sector_id:19)

Fotosector.create(foto:"s18b.jpg", sector_id:18)
Fotosector.create(foto:"s18m.jpg", sector_id:18)
Fotosector.create(foto:"s18c.jpg", sector_id:18)

Fotosector.create(foto:"s17b.jpg", sector_id:17)
Fotosector.create(foto:"s17m.jpg", sector_id:17)
Fotosector.create(foto:"s17c.jpg", sector_id:17)

Fotosector.create(foto:"s16b.jpg", sector_id:16)
Fotosector.create(foto:"s16m.jpg", sector_id:16)
Fotosector.create(foto:"s16c.jpg", sector_id:16)

Fotosector.create(foto:"s15b.jpg", sector_id:15)
Fotosector.create(foto:"s15m.jpg", sector_id:15)
Fotosector.create(foto:"s15c.jpg", sector_id:15)

Fotosector.create(foto:"s14b.jpg", sector_id:14)
Fotosector.create(foto:"s14m.jpg", sector_id:14)
Fotosector.create(foto:"s14c.jpg", sector_id:14)

Fotosector.create(foto:"s13b.jpg", sector_id:13)
Fotosector.create(foto:"s13m.jpg", sector_id:13)
Fotosector.create(foto:"s13c.jpg", sector_id:13)

Fotosector.create(foto:"s50b.jpg", sector_id:49)
Fotosector.create(foto:"s50c.jpg", sector_id:49)

Fotosector.create(foto:"s49b.jpg", sector_id:48)
Fotosector.create(foto:"s49c.jpg", sector_id:48)

Fotosector.create(foto:"s48b.jpg", sector_id:47)
Fotosector.create(foto:"s48c.jpg", sector_id:47)

Fotosector.create(foto:"s47b.jpg", sector_id:46)
Fotosector.create(foto:"s47c.jpg", sector_id:46)

Fotosector.create(foto:"s46b.jpg", sector_id:45)
Fotosector.create(foto:"s46c.jpg", sector_id:45)

Fotosector.create(foto:"s45b.jpg", sector_id:44)
Fotosector.create(foto:"s45c.jpg", sector_id:44)

Fotosector.create(foto:"s44b.jpg", sector_id:43)
Fotosector.create(foto:"s44c.jpg", sector_id:43)

Fotosector.create(foto:"s43b.jpg", sector_id:42)
Fotosector.create(foto:"s43c.jpg", sector_id:42)

Fotosector.create(foto:"s42b.jpg", sector_id:41)
Fotosector.create(foto:"s42c.jpg", sector_id:41)

#Fotos Auditório FEUP
Fotosector.create(foto:"a01.jpg", sector_id:50)
Fotosector.create(foto:"a02.jpg", sector_id:51)
Fotosector.create(foto:"a03.jpg", sector_id:52)
Fotosector.create(foto:"a04.jpg", sector_id:53)
Fotosector.create(foto:"a05.jpg", sector_id:54)
Fotosector.create(foto:"a06.jpg", sector_id:55)

#Comentarios
Comentario.destroy_all
ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'comentarios'")