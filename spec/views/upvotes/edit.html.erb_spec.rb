require 'rails_helper'

RSpec.describe "upvotes/edit", :type => :view do
  before(:each) do
    @upvote = assign(:upvote, Upvote.create!(
      :user_id => 1,
      :fotosector_id => 1
    ))
  end

  it "renders the edit upvote form" do
    render

    assert_select "form[action=?][method=?]", upvote_path(@upvote), "post" do

      assert_select "input#upvote_user_id[name=?]", "upvote[user_id]"

      assert_select "input#upvote_fotosector_id[name=?]", "upvote[fotosector_id]"
    end
  end
end
