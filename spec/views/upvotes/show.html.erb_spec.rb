require 'rails_helper'

RSpec.describe "upvotes/show", :type => :view do
  before(:each) do
    @upvote = assign(:upvote, Upvote.create!(
      :user_id => 1,
      :fotosector_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
  end
end
