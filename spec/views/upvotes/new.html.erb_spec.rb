require 'rails_helper'

RSpec.describe "upvotes/new", :type => :view do
  before(:each) do
    assign(:upvote, Upvote.new(
      :user_id => 1,
      :fotosector_id => 1
    ))
  end

  it "renders new upvote form" do
    render

    assert_select "form[action=?][method=?]", upvotes_path, "post" do

      assert_select "input#upvote_user_id[name=?]", "upvote[user_id]"

      assert_select "input#upvote_fotosector_id[name=?]", "upvote[fotosector_id]"
    end
  end
end
