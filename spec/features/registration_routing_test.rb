require 'spec_helper'
require "rails_helper"

describe "Routing Registration" do
	
	it "go back from sign up", :js => true do
       visit "/users/sign_up"
	   find('#back').click
	   expect("/").to eq(current_path)
    end
	
	it "go home from sign up", :js => true do
       visit "/users/sign_up"
	   find('#log').click
	   expect("/").to eq(current_path)
    end
	
end