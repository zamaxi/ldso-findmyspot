require 'spec_helper'
require "rails_helper"

  describe "Routes" do
  
	it "go to dragon stadium", :js => true do
       visit "/"
	   find('a', text: 'abc').click
	   expect("/recintos/1").to eq(current_path)
    end
	
	it "go to sign up", :js => true do
       visit "/"
	   find('a', text: 'Sign up').click
	   expect("/users/sign_up").to eq(current_path)
    end
	
  end
