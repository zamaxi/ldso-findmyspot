require 'spec_helper'
require "rails_helper"

describe "Routing Stadium" do
  
	it "go to sign up", :js => true do
       visit "/recintos/1"
	   find('a', text: 'Sign up').click
	   expect("/users/sign_up").to eq(current_path)
    end
	
	it "go back from stadium", :js => true do
       visit "/recintos/1"
	   find('#back').click
	   expect("/").to eq(current_path)
    end
	
	it "go home from stadium", :js => true do
       visit "/recintos/1"
	   find('#log').click
	   expect("/").to eq(current_path)
    end
	
end
