require 'spec_helper'
require "rails_helper"

describe "Stadium" do
 
	it "click image and see", :js => true do
       visit "/recintos/1"
	   page.execute_script('$("#s01").trigger("mouseover")')
	   find('#s01').click
	   find('.mfp-close')
	   expect("/recintos/1").to eq(current_path)
    end
	
	it "change calendar month", :js => true do
       visit "/recintos/1"
	   find('a', text:'December 2014')
	   expect("/recintos/1").to eq(current_path)
    end 
	
end