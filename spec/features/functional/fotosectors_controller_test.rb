require 'test_helper'

class FotosectorsControllerTest < ActionController::TestCase
  setup do
    @fotosector = fotosectors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fotosectors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fotosector" do
    assert_difference('Fotosector.count') do
      post :create, fotosector: { foto: @fotosector.foto }
    end

    assert_redirected_to fotosector_path(assigns(:fotosector))
  end

  test "should show fotosector" do
    get :show, id: @fotosector
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fotosector
    assert_response :success
  end

  test "should update fotosector" do
    put :update, id: @fotosector, fotosector: { foto: @fotosector.foto }
    assert_redirected_to fotosector_path(assigns(:fotosector))
  end

  test "should destroy fotosector" do
    assert_difference('Fotosector.count', -1) do
      delete :destroy, id: @fotosector
    end

    assert_redirected_to fotosectors_path
  end
end
