require 'test_helper'

class BancadasControllerTest < ActionController::TestCase
  setup do
    @bancada = bancadas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bancadas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bancada" do
    assert_difference('Bancada.count') do
      post :create, bancada: { nome: @bancada.nome }
    end

    assert_redirected_to bancada_path(assigns(:bancada))
  end

  test "should show bancada" do
    get :show, id: @bancada
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bancada
    assert_response :success
  end

  test "should update bancada" do
    put :update, id: @bancada, bancada: { nome: @bancada.nome }
    assert_redirected_to bancada_path(assigns(:bancada))
  end

  test "should destroy bancada" do
    assert_difference('Bancada.count', -1) do
      delete :destroy, id: @bancada
    end

    assert_redirected_to bancadas_path
  end
end
