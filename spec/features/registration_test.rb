require 'spec_helper'
require "rails_helper"

describe "Register" do
 
	it "registration sucessfull", :js => true do
       visit "/users/sign_up"
	   fill_in("user_email", with: 'nuno@gmail.com', :match => :prefer_exact)
	   fill_in("user_password", with: 'lalslslslslslsl', :match => :prefer_exact)
       fill_in "user_password_confirmation", :with => 'lalslslslslslsl'
	   find('#registrationdone').click
	   expect("/").to eq(current_path)
    end
	
	it "registration not sucessfull", :js => true do
       visit "/users/sign_up"
	   fill_in("user_email", with: 'nuno@gmail.com', :match => :prefer_exact)
	   fill_in("user_password", with: 'lal', :match => :prefer_exact)
       fill_in "user_password_confirmation", :with => 'lal'
	   find('#registrationdone').click
	   expect("/users").to eq(current_path)
	   find('#error_explanation')
    end
end